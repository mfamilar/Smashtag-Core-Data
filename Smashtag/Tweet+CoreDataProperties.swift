//
//  Tweet+CoreDataProperties.swift
//  Smashtag
//
//  Created by Marc FAMILARI on 2/22/17.
//  Copyright © 2017 Marc FAMILARI. All rights reserved.
//

import Foundation
import CoreData


extension Tweet {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tweet> {
        return NSFetchRequest<Tweet>(entityName: "Tweet");
    }

    @NSManaged public var text: String?
    @NSManaged public var posted: NSDate?
    @NSManaged public var unique: String?
    @NSManaged public var tweeter: TwitterUser?

}
